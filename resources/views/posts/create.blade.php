@extends('layouts.app')

@section('title', 'Create Post')

@section('content')
    <div class="container">
        <form method="POST" action="{{ route('posts.store') }}" role="form">

            @csrf

            {{-- Post Title --}}
            <div class="form-group">
                <label for="title">Post Title</label>

                <input class="form-control{{ $errors->has('title') ? ' is-invalid' : null }}" type="text" name="title" id="title" placeholder="Post title" value="{{ old('title') }}" autofocus>

                @if ($errors->has('title'))
                    <div class="text-danger">{{ $errors->first('title') }}</div>
                @endif
            </div>

            {{-- Post Content --}}
            <div class="form-group">
                <label for="content">Post Content</label>

                <textarea class="form-control{{ $errors->has('content') ? ' is-invalid' : null }}" name="content" id="content" rows="5" placeholder="Post content">{{ old('content') }}</textarea>

                @if ($errors->has('content'))
                    <div class="text-danger">{{ $errors->first('content') }}</div>
                @endif
            </div>

            {{-- Button to submit the form --}}
            <input class="btn btn-primary" type="submit" value="Create Post">
        </form>
    </div>
@endsection
