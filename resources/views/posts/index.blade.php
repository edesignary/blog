@extends('layouts.app')

@section('title', 'Blog')

@section('content')
    <div class="container">
        @if (!$posts->count())
            <div class="text-center">
                <h3>It's empty here!</h3>

                @auth
                    <a class="btn btn-info" href="{{ route('posts.create') }}">Create Post</a>
                @endauth
            </div>
        @else
            <ul class="list-group">
                @foreach ($posts as $post)
                    <li class="list-group-item">

                        {{-- Only logged in users can edit and delete the posts --}}

                        @auth
                            <div class="d-inline pr-3">
                                {{-- Edit Post --}}
                                <a class="btn btn-primary btn-sm" href="{{ route('posts.edit', $post->slug) }}">
                                    <i class="fa fa-pencil"></i>
                                </a>

                                {{-- Delete Post --}}
                                <form class="d-inline" method="POST" action="{{ route('posts.destroy', $post->slug) }}" onsubmit="if (!confirm('Are you sure to delete this post?')) { return false; }">

                                    @csrf

                                    @method('DELETE')

                                    <button class="btn btn-danger btn-sm">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </form>
                            </div>
                        @endauth

                        {{-- Post Created Time --}}
                        <div class="d-inline">
                            <i class="fa fa-clock-o"></i>

                            <span class="text-muted">
                                {{ \Carbon\Carbon::parse($post->created_at)->diffForHumans() }}
                            </span>
                        </div>

                        {{-- Post Title --}}
                        <h1 class="h6 d-inline pl-3">
                            <a href="{{ route('posts.show', ['post' => $post->slug]) }}">
                                {{ $post->title }}
                            </a>
                        </h1>

                        {{-- Post Content --}}
                        <p class="text-muted pt-3">
                            {{ str_limit($post->content, 150) }}  
                        </p>
                    </li>
                @endforeach
            </ul>

            {{-- Pagination Links --}}
            <div class="my-3">
                {{ $posts->links() }}
            </div>
        @endif
    </div>
@endsection
