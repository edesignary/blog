@extends('layouts.app')

@section('title', $post->title)

@section('content')
    <div class="container bg-white p-5 shadow-sm">
        {{-- Post Created Time --}}
        <div class="d-inline">
            <i class="fa fa-calendar"></i>
            <span class="text-muted">
                {{ \Carbon\Carbon::parse($post->created_at)->format("D, dS M'y") }}
            </span>
        </div>

        {{-- Post Title --}}
        <h1 class="h3 pt-4">
            {{ $post->title }}
        </h1>

        {{-- Post Content --}}
        <p class="text-muted pt-3">
            {{ $post->content }}  
        </p>
    </div>
@endsection
