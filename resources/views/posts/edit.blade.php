@extends('layouts.app')

@section('title', 'Edit Post')

@section('content')
    <div class="container">
        <form method="POST" action="{{ route('posts.update', $post->slug) }}" role="form">

            @csrf

            @method('PATCH')

            {{-- Post Title --}}
            <div class="form-group">
                <label for="title">Post Title</label>

                <input class="form-control{{ $errors->has('title') ? ' is-invalid' : null }}" type="text" name="title" id="title" placeholder="Post title" value="{{ old('title', $post->title) }}" autofocus
                >

                @if ($errors->has('title'))
                    <div class="text-danger">{{ $errors->first('title') }}</div>
                @endif
            </div>

            {{-- Post Content --}}
            <div class="form-group">
                <label for="content">Post Content</label>

                <textarea class="form-control{{ $errors->has('content') ? ' is-invalid' : null }}" name="content" id="content" rows="5" placeholder="Post content">{{ old('content', $post->content) }}</textarea>

                @if ($errors->has('content'))
                    <div class="text-danger">{{ $errors->first('content') }}</div>

                @endif
            </div>

            {{-- Button to submit the form --}}
            <input class="btn btn-info" type="submit" value="Update Post">
        </form>
    </div>
@endsection
